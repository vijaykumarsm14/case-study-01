﻿using System;

public class Employee
{
    public void EmployeeSalary()
    {
        Double Basic_Salary, Allowance, Gross_Salary;
        int experience;

        Console.Write("Enter the Experience in Years:");
        experience = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter Basic Salary : ");
        Basic_Salary = Convert.ToInt32(Console.ReadLine());

        if (experience <= 2)
        {
            Allowance = (Basic_Salary * 30) / 100;
            Gross_Salary = Basic_Salary + Allowance;
            Console.Write("The Gross salary of the Employee is:" + Gross_Salary);
        }
        else if(experience <= 4)
        {
            Allowance = (Basic_Salary * 40) / 100;
            Gross_Salary = Basic_Salary + Allowance;
            Console.Write("The Gross salary of the Employee is:" + Gross_Salary);
        }

        else if (experience <= 6)
        {
            Allowance = (Basic_Salary * 50) / 100;
            Gross_Salary = Basic_Salary + Allowance;
            Console.Write("The Gross salary of the Employee is:" + Gross_Salary);
        }

        else if(experience > 6 )
        {
            Allowance = (Basic_Salary * 65) / 100;
            Gross_Salary = Basic_Salary + Allowance;
            Console.Write("The Gross salary of the Employee is:" + Gross_Salary);
        }

    }
    public class Code
    {
        public static void Main()
        {
            Employee emp = new Employee();
            emp.EmployeeSalary();


        }
    }
}